/*
 * Copyright 2019, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.android.marsrealestate.network

import com.example.android.marsrealestate.Constants.Companion.BASE_URL
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

private const val BASE_URL =
  "https://android-kotlin-fun-mars-server.appspot.com"

private val moshi = Moshi.Builder()
  .add(KotlinJsonAdapterFactory())
  .build()

/** Call adapters add the ability for Retrofit to create APIs that
 *  return something other than the default Call class.
 *  In this case, the CoroutineCallAdapterFactory allows us to
 *  replace the Call object that getProperties() returns with a Deferred object instead. */
private val retrofit = Retrofit.Builder()
  .baseUrl(BASE_URL)
  .addConverterFactory(MoshiConverterFactory.create(moshi))
  .addCallAdapterFactory(CoroutineCallAdapterFactory())
  .build()


/** The Deferred interface defines a coroutine job that returns a result value (Deferred inherits from Job).
 *  The Deferred interface includes a method called await(), which causes code
 *  to wait without blocking until the value is ready,
 *  and then that value is returned.
 *  */
interface MarsApiService {
  @GET("realestate")
  fun getProps(): Deferred<List<MarsProperty>>
}

object MarsApi {

  val retrofitInstance: MarsApiService by lazy {
    retrofit.create(
      MarsApiService::class.java
    )
  }

}